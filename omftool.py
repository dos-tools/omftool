#!/usr/bin/env python

import sys
import os
import bidict

record_map = bidict.bidict({
    0x70 : "Register Initialization",
    0x7a : "Block Definition",
    0x7c : "Block End",
    0x7e : "Debug Symbols",
    0x80 : "Translator Header",
    0x88 : "Comment",
    0x8a : "Module End",
    0x8c : "External Names Definition",
    0x8e : "Type Definition",
    0x90 : "Public Names Definition",
    0x94 : "Line Numbers",
    0x96 : "List of Names",
    0x98 : "Segment Definition",
    0x9a : "Group Definition",
    0x9c : "Fixup",
    0xa0 : "Logical Enumerated Data"})

# read record, return contents
def read_record(f, record_type):

    # read record length
    record_length = int.from_bytes(f.read(2), "little")

    # read record contents
    record_contents = f.read(record_length-1)
    contents_sum = sum(list(record_contents))

    # read checksum
    checksum = int.from_bytes(f.read(1), "little")

    # verify checksum
    if (record_type + record_length + contents_sum + checksum) % 0x100 != 0:
        print("checksum problem")
        exit(-1)
    return record_contents

def values_in_range(array, minimum, maximum):
    for x in array:
        if x < minimum or x >= maximum:
            return False
    return True

def print_contents(contents, output):
    i = 0
    first = True
    newline = True
    while i < len(contents):
        ix = i+1+contents[i]
        if contents[i] in range(4, 32) \
                and ix <= len(contents) \
                and values_in_range(s := contents[i+1:ix], 0x20, 0x7f):
            if not first:
                print(file=output)
                newline = True
            if not newline:
                print(' ', end='', file=output)
            print("'{}'".format(s.decode()), end='', file=output)
            newline = False
            i += contents[i]+1
        else:
            if not newline:
                print(' ', end='', file=output)
            print("{:02x}".format(contents[i]), end='', file=output)
            newline = False
            i += 1
            if i > 0 and i % 16 == 0:
                print(file=output)
                newline = True
        first = False
    print("\n", file=output)

# convert to text representation
def omf_to_text(f_in, f_out):
    while (rt := f_in.read(1)) != b'':
        record_type = int.from_bytes(rt, "little")
        record_contents = read_record(f_in, record_type)
        print(record_map.get(record_type, hex(record_type)), file=f_out)
        print_contents(record_contents, output=f_out)

def line_to_bytes(line):
    in_string = False
    hex_string = ""
    result = []
    char_string = []
    for c in line:
        if in_string:
            if c == "'":
                in_string = False
                result.append(len(char_string))
                result += char_string
            else:
                char_string.append(ord(c))
        else:
            if c.isalnum():
                hex_string += c
            else:
                if hex_string != "":
                    result.append(int(hex_string, 16))
                    hex_string = ""
                if c == "'":
                    in_string = True
                    char_string = []
    if hex_string != "":
        result.append(int(hex_string, 16))
    return result

def text_to_omf(f_in, f_out):
    lines = f_in.readlines()
    new_record = True
    rec_lines = []
    for l in lines:
        line = l.strip()
        if new_record and len(line) > 0:
            record_type = record_map.inverse[line]
            new_record = False
            rec_lines = []
        elif len(line) == 0 and not new_record:
            byte_list = line_to_bytes(rec_lines)
            record_length = len(byte_list)+1
            checksum = 0x100 - (record_type + record_length + sum(byte_list)) % 0x100
            f_out.write(record_type.to_bytes(1, 'little'))
            f_out.write(record_length.to_bytes(2, 'little'))
            f_out.write(bytes(byte_list))
            f_out.write(checksum.to_bytes(1, 'little'))
            new_record = True
        else:
            rec_lines += " " + line

def is_omf(filename):
    f = open(filename,'rb')
    b = int.from_bytes(f.read(1), "little")
    f.close()
    return b == 0x80

# main
if len(sys.argv) == 2 or len(sys.argv) == 3:
    fn_in = sys.argv[1]
    if not os.path.isfile(fn_in):
        print("File does not exist: {}".format(fn_in))
        exit(-1)
if len(sys.argv) == 3:
    fn_out = sys.argv[2]
    if is_omf(fn_in):
        f_in = open(fn_in, 'rb')
        f_out = open(fn_out, 'w')
        omf_to_text(f_in, f_out)
        f_in.close()
        f_out.close()
    else:
        f_in = open(fn_in, 'r')
        f_out = open(fn_out, 'wb')
        text_to_omf(f_in, f_out)
        f_in.close()
        f_out.close()
elif len(sys.argv) == 2:
    if is_omf(fn_in):
        f = open(fn_in, 'rb')
        omf_to_text(f, sys.stdout)
        f.close()
    else:
        print("The input file is not OMF")
else:
    print("Tool to convert between OMF and text representation.")
    print("Usage:", end="");
    print("\tomftool <omf>")
    print("\tomftool <omf> <text>")
    print("\tomftool <text> <omf>")
    print("The source file must exist.")

